import axios from 'axios';

export default axios.create({
  baseURL: 'https://api.unsplash.com',
  headers: {
    Authorization:
      'Client-ID kJPtWcYOFAXQBnEo-eU508YHgrdDPB-v65mG6S-NYOc'
  }
});

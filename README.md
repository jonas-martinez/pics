## TP Virtualisation, Cloud ....

### 1 : Préparation du TP
*  Création du repo gitlab en faisant un fork du repo pics (duplication sur trois branches master, develop et recette)
*  Création des applications Heroku avec les noms correspondant à la branche respective (accès par : [text](https://jonas-pics-master.herokuapp.com/))

### 2 : Envoi du projet sur Heroku
* Récupération de la clé d'API et création des variables correspondantes sur le projet gitlab
* Configuration du fichier gitlab-ci.yml